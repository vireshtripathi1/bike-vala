const colors = {
  brand: "#008ba0",
  pink: "#914574",
};

module.exports = {
  content: [
    "./atoms/*.js",
    "./atoms/**/*.js",
    "./components/*.js",
    "./components/**/*.js",
    "./components/**/**/*.js",
    "./components/**/**/**/*.js",
    "./molecules/*.js",
    "./molecules/**/*.js",
    "./molecules/**/**/*.js",
    "./molecules/**/**/**/*.js",
    "./organisms/*.js",
    "./organisms/**/*.js",
    "./organisms/**/**/*.js",
    "./organisms/**/**/**/*.js",
    "./pages/*.js",
    "./pages/**/*.js",
    "./pages/**/**/*.js",
    "./pages/**/**/**/*.js",
    "./templates/*.js",
    "./templates/**/*.js",
    "./templates/**/**/*.js",
    "./templates/**/**/**/*.js",
  ],
  theme: {
    extend: {
      borderColor: () => ({
        ...colors,
      }),

      backgroundColor: () => ({
        ...colors,
      }),

      textColor: () => ({
        ...colors,
      }),
      content: () => ({}),
      backgroundImage: () => ({}),
      gradientColorStops: () => ({}),
      divideColor: () => ({}),
      boxShadow: () => ({}),
      minWidth: () => ({}),
      height: () => ({}),
      minHeight: () => ({}),
      maxHeight: () => ({}),
      width: () => ({}),
      maxWidth: () => ({}),
      borderRadius: () => ({}),
      fontSize: () => ({}),
      zIndex: () => ({}),
      margin: {},
    },
  },
};
