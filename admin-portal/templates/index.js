import Dashboard from "./Dashboard";
import ProductListTemplate from "./products/ProductListTemplate";
import AddProductTemplate from "./products/AddProductTemplate";

export { Dashboard, ProductListTemplate, AddProductTemplate };
