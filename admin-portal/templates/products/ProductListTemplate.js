import DataTable from "react-data-table-component";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

import { Button, Text } from "@/atoms";
import { viewProductAPI } from "@/services/product.service";

const ProductListTemplate = () => {
  const router = useRouter();
  const [productList, setProductList] = useState([]);
  const columns = [
    {
      name: "Name",
      selector: (row) => row.name,
      sortable: true,
    },
    {
      name: "cat_id",
      selector: (row) => row.cat_id,
    },
    {
      name: "short_desc",
      selector: (row) => row.short_desc,
    },
    {
      name: "is_feature",
      selector: (row) => row.is_feature,
    },
    {
      name: "status",
      selector: (row) => row.status,
    },
  ];

  useEffect(() => {
    viewProductAPI({ setProductList });
    console.log("productList.....", productList);
  }, []);

  return (
    <>
      <div className="flex justify-between my-3">
        <Text
          {...{ content: "Product List", className: "text-xl font-semibold" }}
        />
        <Button
          {...{
            label: "Add Product",
            width: "w-32 bg-brand hover:bg-pink text-white rounded py-2",
            onClick: () => {
              router.push("/products/add-product");
            },
          }}
        />
      </div>
      <DataTable
        columns={columns}
        data={productList}
        pagination
        suppressHydrationWarning
      />
    </>
  );
};

export default ProductListTemplate;
