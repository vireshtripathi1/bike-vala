import * as Yup from "yup";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import axios from "axios";

import { Button, InputField, Text } from "@/atoms";
import addProductFormAPI from "@/services/product.service";

const validationSchema = Yup.object().shape({
  name: Yup.string().required("Name is required"),
});

const tabMenuProduct = [
  { label: "Basic Details" },
  { label: "Price" },
  { label: "Feature" },
  { label: "Specification" },
  { label: "Image" },
  { label: "Colors" },
];

const AddProductTemplate = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(validationSchema),
  });

  const onSubmit = (data) => {
    addProductFormAPI(data);
    // console.log("data", data);
  };

  return (
    <div className="flex flex-col justify-between">
      <Text
        {...{
          content: "Add New Product",
          className: "text-xl font-medium",
        }}
      />
      <div className="flex gap-4">
        {tabMenuProduct.map(({ label }, index) => (
          <Button
            key={index}
            {...{
              label,
              className:
                "w-32 flex bg-brand hover:bg-pink text-white rounded py-2",
            }}
          />
        ))}
      </div>
      <form method="post" onSubmit={handleSubmit(onSubmit)}>
        <div className="flex gap-4">
          <InputField
            {...{
              type: "hidden",
              name: "cat_id",
              dbName: "cat_id",
              value: "4f7ecd18-8d77-4d93-8a1d-deee67e0c7d6",
              errors,
              register: register("cat_id"),
            }}
          />
          <InputField
            {...{
              label: "Name",
              placeholder: "Enter Product Name",
              name: "name",
              dbName: "name",
              errors,
              register: register("name"),
            }}
          />
          {errors.name && (
            <p className="text-red-500 text-sm">{errors.name.message}</p>
          )}
          <div className="flex flex-col">
            <label htmlFor="is_feature">Is Feature</label>
            <select
              id="is_feature"
              name="is_feature"
              {...register("is_feature")}
              className="w-full border text-gray-900 px-2 py-2 outline-none placeholder:text-grey-400"
            >
              <option value="0" selected>
                No
              </option>
              <option value="1">Yes</option>
            </select>
          </div>

          <InputField
            {...{
              label: "Short Description",
              placeholder: "Enter Short Description",
              dbName: "short_desc",
              error: {},
              register: register("short_desc"),
            }}
          />

          <InputField
            {...{
              label: "Description",
              placeholder: "Enter Description",
              dbName: "description",
              error: {},
              register: register("description"),
            }}
          />

          <div className="flex flex-col">
            <label htmlFor="status">Status</label>
            <select
              id="status"
              {...register("status")}
              name="status"
              className="w-full border text-gray-900 px-2 py-2 outline-none placeholder:text-grey-400"
            >
              <option value="0">No</option>
              <option value="1" selected>
                Yes
              </option>
            </select>
          </div>
        </div>
        <Button
          {...{
            label: "Add Product",
            className:
              "w-32 flex bg-brand hover:bg-pink text-white rounded py-2",
          }}
        />
      </form>
    </div>
  );
};

export default AddProductTemplate;
