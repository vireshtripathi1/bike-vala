export const SIDEBAR_MENU = [
  {
    label: "Home",
    href: "/home",
    icon: "",
  },
  {
    label: "Products",
    href: "/products",
    icon: "",
  },
  {
    label: "Categories",
    href: "categories",
    icon: "",
  },
  {
    label: "Offers",
    href: "offers",
    icon: "",
  },
  {
    label: "Enquires",
    href: "enquires",
    icon: "",
  },
  {
    label: "Customers",
    href: "customers",
    icon: "",
  },
  {
    label: "Logout",
    href: "logout",
    icon: "",
  },
];
