import { useRouter } from "next/router";

import PageNotFound from "@/molecules";
import { HeaderOrganism, SideBarOrganism } from "@/organisms";

const StandardLayout = ({
  children,
  mainStyle = "md:mt-16",
  pageDataFound = true,
  showHeader = true,
}) => {
  const router = useRouter();

  return (
    <>
      {pageDataFound ? (
        <div
          className="flex flex-col justify-between h-screen"
          id="standard-layout-wrapper"
        >
          <div>
            {showHeader && <HeaderOrganism {...{ className: "w-full" }} />}
            <main className={`${mainStyle}`} id="main-content">
              <div className="flex">
                <SideBarOrganism />
                <div className="ml-72 bg-gray-100 w-full h-[93vh] p-6">
                  {children}
                </div>
              </div>
            </main>
          </div>
        </div>
      ) : (
        <PageNotFound />
      )}
    </>
  );
};

export default StandardLayout;
