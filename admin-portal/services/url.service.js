import { BASE_URL } from "./constent.service";

export const ADD_PRODUCT_URI = `${BASE_URL}/product/add-product`;
export const VIEW_PRODUCT_URL = `${BASE_URL}/product/list`;
