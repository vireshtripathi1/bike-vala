import * as HttpService from "./http.service";
import { ADD_PRODUCT_URI, VIEW_PRODUCT_URL } from "./url.service";

const addProductForm = ({ data }) =>
  HttpService.postWithAuth(ADD_PRODUCT_URI, data);

const getProduct = () => HttpService.getWithoutAuth(VIEW_PRODUCT_URL);

export const addProductFormAPI = (data) => {
  addProductForm({ data })
    .then((response) => {
      console.log("response_Data", data);
    })
    .catch((error) => console.log("error", error));
};

export const viewProductAPI = async ({ setProductList }) => {
  // try {
  //   const data = await getProduct();
  //   if (data) {
  //     console.log("PRODUCT_LIST", data);
  //     // setProductList(data);
  //   }
  // } catch (error) {
  //   console.log("Error", error);
  // }
  getProduct()
    .then((response) => {
      console.log("PRODUCT_LIST", response.data);
      setProductList(response.data);
      return "response";
    })
    .catch((error) => console.log("PRODUCT_LIST__ERROR", error));
};
