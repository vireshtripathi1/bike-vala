import axios from "axios";

export const postWithoutAuth = (url, entity) =>
  new Promise((resolve, reject) => {
    axios
      .post(url, entity)
      .then((response) => {
        if (response?.data) {
          resolve(response.data);
        }
      })
      .catch((ex) => {
        reject(ex);
      });
  });

export const postWithAuth = (url, entity) =>
  new Promise((resolve, reject) => {
    axios
      .post(url, entity, "/token")
      .then((response) => {
        if (response?.data) {
          resolve(response.data);
        }
      })
      .catch((ex) => {
        reject(ex);
      });
  });

export const getWithoutAuth = (url) =>
  new Promise((resolve, reject) => {
    axios
      .get(url)
      .then((response) => {
        if (response?.data) {
          resolve(response.data);
        }
      })
      .catch((ex) => {
        reject(ex);
      });
  });
