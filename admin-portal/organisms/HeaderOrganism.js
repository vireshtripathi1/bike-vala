import { Text } from "@/atoms";

const HeaderOrganism = () => (
  <div className="fixed top-0 w-full flex items-center px-6 justify-between bg-white h-16 shadow">
    <Text {...{ content: "Admin Panel", className: "text-lg font-semibold" }} />
    <Text {...{ content: "Logout", className: "text-lg font-semibold" }} />
  </div>
);

export default HeaderOrganism;
