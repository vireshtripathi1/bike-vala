import { Text } from "@/atoms";
import { SIDEBAR_MENU } from "@/config/menu";
import Link from "next/link";

const SideBarOrganism = () => (
  <div className="bg-black text-white w-72 h-screen fixed">
    {SIDEBAR_MENU.map(({ label, href, icon }, index) => (
      <Link className="block gap-2 my-2 mx-2" key={index} href={href}>
        <Text
          {...{
            content: label,
            HtmlTag: "label",
            className:
              "block text-black bg-white hover:bg-brand hover:text-white px-4 rounded py-2 text-medium cursor-pointer",
          }}
        />
      </Link>
    ))}
  </div>
);

export default SideBarOrganism;
