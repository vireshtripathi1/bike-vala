import Image from "next/image";

// import { staticMediaStoreBaseURL } from '@/config/common';
const staticMediaStoreBaseURL = "";
const TextWithIcon = ({
  alt = "icon",
  className = "shadow-card py-1 px-2 rounded",
  icon,
  iconHeight = 16,
  iconStyle,
  iconWidth = 16,
  label,
  labelStyle = "text-sm text-nero ",
  onClick = () => {},
  show = true,
}) =>
  show && (
    <div onClick={onClick}>
      <div className={`flex gap-2 items-center ${className}`}>
        <Image
          alt={alt}
          className={iconStyle}
          height={0}
          src={`${staticMediaStoreBaseURL}/icons/${icon}`}
          style={{ height: iconHeight, width: iconWidth }}
          width={0}
        />
        <div className={labelStyle}>{label}</div>
      </div>
    </div>
  );

export default TextWithIcon;
