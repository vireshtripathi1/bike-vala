import StandardLayout from "@/components/layout/StandardLayout";
import { Dashboard } from "@/templates";

const Home = () => (
  <StandardLayout>
    <Dashboard />
  </StandardLayout>
);

export default Home;
