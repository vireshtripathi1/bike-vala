import StandardLayout from "@/components/layout/StandardLayout";
import { AddProductTemplate } from "@/templates";

const AddProduct = () => (
  <StandardLayout>
    <AddProductTemplate />
  </StandardLayout>
);

export default AddProduct;
