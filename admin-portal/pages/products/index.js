import StandardLayout from "@/components/layout/StandardLayout";
import { ProductListTemplate } from "@/templates";

const Index = () => (
  <StandardLayout>
    <ProductListTemplate />
  </StandardLayout>
);

export default Index;
