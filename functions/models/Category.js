"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Category extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {}
  }
  Category.init(
    {
      id: {
        allowNull: false,
        primaryKey: true,
        defaultValue: DataTypes.UUIDV4,
        type: DataTypes.UUID,
      },
      cat_name: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      is_parent: {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.INTEGER,
      },
      cat_image: {
        type: DataTypes.TEXT,
      },
      is_featured: {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.INTEGER,
      },
      status: {
        allowNull: false,
        defaultValue: 1,
        type: DataTypes.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "Category",
      tableName: "product_categories",
    }
  );
  return Category;
};
