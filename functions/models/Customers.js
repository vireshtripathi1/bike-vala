"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Customers extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  Customers.init(
    {
      id: { type: DataTypes.UUID, primaryKey: true },
      name: { type: DataTypes.STRING },
      mobile: { type: DataTypes.INTEGER },
      dob: { type: DataTypes.STRING },
      email: {
        type: DataTypes.STRING,
        unique: true,
        validate: {
          isEmail: true, // checks for email format
        },
      },
      password: { type: DataTypes.STRING },
      gender: { type: DataTypes.INTEGER },
      address: { type: DataTypes.STRING },
      pincode: { type: DataTypes.INTEGER },
      city: { type: DataTypes.STRING },
      state: { type: DataTypes.STRING },
      profile_img: { type: DataTypes.STRING },
      status: { type: DataTypes.INTEGER },
    },
    {
      sequelize,
      modelName: "Customers",
      tableName: "customers",
    }
  );
  Customers.beforeCreate(async (data, options) => {
    const salt = await bcrypt.genSalt(10);
    data.password = await bcrypt.hash(data.password, salt);
  });
  return Customers;
};
