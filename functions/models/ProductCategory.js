"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ProductCategory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.hasMany(models.Product, {
        foreignKey: "cat_id",
      });
    }
  }
  ProductCategory.init(
    {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.UUID,
      },
      cat_name: {
        allowNull: false,
        type: DataTypes.STRING,
      },
      is_parent: {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.NUMBER,
      },
      cat_image: {
        type: DataTypes.TEXT,
      },
      is_featured: {
        allowNull: false,
        defaultValue: 0,
        type: DataTypes.NUMBER,
      },
      status: {
        allowNull: false,
        defaultValue: 1,
        type: DataTypes.NUMBER,
      },

      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
    },
    {
      sequelize,
      modelName: "ProductCategory",
      tableName: "product_categories",
    }
  );
  return ProductCategory;
};
