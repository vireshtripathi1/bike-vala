const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const { sequelize } = require("./models");
const customerRoutes = require("./controllers/customerController");
const productRoutes = require("./controllers/productController");

const app = express();

// Middleware setup
app.use(bodyParser.json());
app.use(cors());

app.use("/api", customerRoutes);
app.use("/api", productRoutes);

app.get("/", (req, res) => {
  res.send("Hello, world!");
});

// Start the server
const port = process.env.PORT || 3005;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
