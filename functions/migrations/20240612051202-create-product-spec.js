"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("product_specs", {
      id: {
        allowNull: false,
        defaultValue: Sequelize.UUIDV4,
        type: Sequelize.UUID,
      },
      product_id: {
        allowNull: false,
        type: Sequelize.UUID,
        references: {
          model: "products",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "SET NULL",
      },
      displacement: {
        type: Sequelize.STRING,
      },
      max_torque: {
        type: Sequelize.STRING,
      },
      fuel_capacity: {
        type: Sequelize.INTEGER,
      },
      milage: {
        type: Sequelize.STRING,
      },
      front_brake: {
        type: Sequelize.STRING,
      },
      rear_break: {
        type: Sequelize.STRING,
      },
      warranty: {
        type: Sequelize.STRING,
      },
      status: {
        allowNull: false,
        defaultValue: 1,
        type: Sequelize.INTEGER,
      },

      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("product_specs");
  },
};
