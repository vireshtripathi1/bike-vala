"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("customers", {
      id: {
        type: Sequelize.UUID,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
      },
      name: { type: Sequelize.STRING },
      mobile: { type: Sequelize.BIGINT },
      dob: { type: Sequelize.STRING },
      email: { type: Sequelize.STRING },
      gender: { type: Sequelize.INTEGER },
      address: { type: Sequelize.STRING },
      pincode: { type: Sequelize.INTEGER },
      city: { type: Sequelize.STRING },
      state: { type: Sequelize.STRING },
      profile_img: { type: Sequelize.STRING },
      status: { defaultValue: 1, type: Sequelize.INTEGER },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("customers");
  },
};
