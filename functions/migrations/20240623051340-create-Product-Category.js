"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("product_categories", {
      id: {
        allowNull: false,
        primaryKey: true,
        defaultValue: Sequelize.UUIDV4,
        type: Sequelize.UUID,
      },
      cat_name: {
        allowNull: false,
        type: Sequelize.STRING,
      },
      is_parent: {
        allowNull: false,
        defaultValue: 0,
        type: Sequelize.INTEGER,
      },
      cat_image: {
        type: Sequelize.TEXT,
      },
      is_featured: {
        allowNull: false,
        defaultValue: 0,
        type: Sequelize.INTEGER,
      },
      status: {
        allowNull: false,
        defaultValue: 1,
        type: Sequelize.INTEGER,
      },

      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("product_categories");
  },
};
