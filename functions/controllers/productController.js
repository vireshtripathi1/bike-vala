const express = require("express");
const { Product } = require("../models");

const router = express.Router();

router.get("/product/list", async (req, res) => {
  try {
    const data = await Product.findAll();
    return res.status(200).json({
      status: 200,
      data: data,
      message: "successfully",
    });
  } catch (error) {
    return res
      .status(500)
      .json({ msg: error, error: "Internal.... server error" });
  }
});

router.get("/product/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const data = await Product.findByPk(id);
    if (!id) {
      return res.status(404).json({ error: "data not found" });
    }
    return res.status(200).json({
      status: 200,
      data: data,
      message: "successfully",
    });
  } catch (error) {
    return res.status(500).json({ error: "Internal server error" });
  }
});

router.delete("/product/delete/:id", (req, res) => {
  const { id } = req.params;
  const index = Product.findIndex((item) => item.id === id);

  if (index !== -1) {
    Product.splice(index, 1);
    res.status(200).send({ message: "Item deleted successfully" });
  } else {
    res.status(404).send({ message: "Item not found" });
  }
});

router.post("/product/add-product", async (req, res) => {
  const { cat_id, description, id, is_feature, name, short_desc, status } =
    req.body;
  try {
    const newProduct = await Product.create({
      cat_id,
      description,
      id,
      is_feature,
      name,
      short_desc,
      status,
    });
    res.status(201).json({ data: newProduct, message: "Done" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: error, message: "Internal Server Error" });
  }
});

// // PUT endpoint to update an existing item by id
// app.put("/items/:id", (req, res) => {
//   const { id } = req.params;
//   const { name } = req.body;
//   const index = items.findIndex((item) => item.id === id);

//   if (index !== -1) {
//     items[index].name = name;
//     res.status(200).json(items[index]);
//   } else {
//     res.status(404).send({ message: "Item not found" });
//   }
// });

module.exports = router;
