const express = require("express");
const { Category } = require("../modals");

const router = express.Router();

router.get("/category/list", async (req, res) => {
  try {
    const data = Category.findAll();
    return res.status(200).json({
      status: 200,
      data: data,
      message: "successfully",
    });
  } catch (error) {
    return res.status("500").json({ error: error, message: "Error" });
  }
});

router.post("/category/add", async (req, res) => {
  try {
    const { cat_image, cat_name, is_featured, is_parent, status } = req.body;
    const data = await Category.Create({
      cat_image,
      cat_name,
      is_featured,
      is_parent,
      status,
    });

    return res.status(200).json({
      status: 200,
      data: data,
      message: "successfully",
    });
  } catch (error) {
    return res.status("500").json({ error: error, message: "Error" });
  }
});
