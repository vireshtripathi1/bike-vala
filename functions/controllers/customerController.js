require("dotenv").config();
const { Customers } = require("../models");
const express = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const router = express.Router();

router.post("/customer/login", async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await Customers.findOne({ where: { email } });

    if (!user || !(await bcrypt.compare(password, user.password))) {
      return res.status(401).send({ error: "Login failed!" });
    }

    const token = jwt.sign({ id: user.id }, process.env.JWT_SECRET);
    res.send({ user, token });
  } catch (e) {
    res.status(400).send({ error: e.message });
  }
});

router.get("/customer/user/:id", async (req, res) => {
  try {
    const { id } = req.params;
    const data = await Customers.findByPk(id);
    if (!id) {
      return res.status(404).json({ error: "user not found" });
    }
    return res.status(200).json({
      status: 200,
      data: data,
      message: "user get successfully",
    });
  } catch (error) {
    return res.status(500).json({ error: "Internal server error" });
  }
});

module.exports = router;
