"use strict";

/** @type {import('sequelize-cli').Migration} */
const { Customers } = require("../models");
const { v4: uuidv4 } = require("uuid");
const bcrypt = require("bcryptjs");
module.exports = {
  async up(queryInterface, Sequelize) {
    const salt = await bcrypt.genSalt(10);
    const customerData = [
      {
        id: uuidv4(),
        name: "Viresh Tripathi",
        email: "vireshtripathi@gmail.com",
        password: await bcrypt.hash("1234", salt),
        gender: 1,
        mobile: "9519914555",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ];

    // Use Promise.all to wait for all records to be created or found
    await Promise.all(
      customerData.map(async (user) => {
        // Find or create the record based on the user name
        try {
          const [createdUser, created] = await Customers.findOrCreate({
            where: { email: user.email },
            defaults: user, // Data to insert if the record doesn't exist
          });

          if (created) {
            console.log(`Created user: ${createdUser.name}`);
          } else {
            console.log(`Skipped existing user: ${createdUser.name}`);
          }
        } catch (error) {
          console.error(`Error creating user: ${user.name}`, error);
        }
      })
    );
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.bulkDelete("customers", null, {});
  },
};
